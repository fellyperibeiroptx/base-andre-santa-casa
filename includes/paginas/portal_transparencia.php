<section class='bloco-conteudo'>
	<div class='bloco-conteudo-padding bloco-conteudo-conteudo'>
		<div class='row'>
			<div class='col-md-1'></div>
			<div class='col-md-10 container-pdf'>
				

				<?php

					$getBalancos = new Read(); 
					$getBalancos->fullRead("SELECT * FROM ".PREFIX."balanco ORDER BY ano DESC"); 
					if($getBalancos->getResult()){
						foreach ($getBalancos->getResult() AS $key => $item) {
							
							echo "<div class='item-pdf' style='margin-bottom: 40px;'>";
							echo "<div class='img-pdf'>";
							echo "<?xml version='1.0' encoding='utf-8'?>
									<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
									<svg version='1.1' id='Camada_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'
										 viewBox='0 0 374 506' style='enable-background:new 0 0 374 506;' xml:space='preserve'>
									<style type='text/css'>
										.st0{fill:#F3F3F3;}
										.st1{fill:#DBDBDB;}
										.st2{fill:#29B6F6;}
										.st3{fill:#FFFFFF;}
										.st4{font-family: 'Montserrat', sans-serif;}
										.st5{font-size:75.3294px;}
										.st6{fill:#E53935;}
									</style>
									<path class='st0' d='M368,114v270H6.5V29.3C6.5,15.9,17,5,30,5h232.6L368,114z'/>
									<path class='st1' d='M260,11l102,105.4V398H12.5V29.3C12.5,19.2,20.3,11,30,11H260 M262.5,5H30C17,5,6.5,15.9,6.5,29.3V404H368V114
										L262.5,5L262.5,5z'/>
									<path class='st1' d='M262.5,89.7c0,13.4,10.5,24.3,23.5,24.3h82L262.5,5V89.7z'/>
									<path class='st2' d='M368,384v96c0,11.6-10.5,21-23.5,21H30c-13,0-23.5-9.4-23.5-21v-96H368z'/>
									<text transform='matrix(1 0 0 1 103.4697 468.0801)' class='st3 st4 st5' class='fonte1'>{$item['ano']}</text>
									<path class='st6' d=\"M123.3,311.4c-2.3,0-4.6-0.8-6.5-2.2c-7-5.3-8-11.1-7.5-15.1c1.2-11,14.8-22.5,40.4-34.2 c10.2-22.3,19.8-49.7,25.5-72.6c-6.7-14.7-13.3-33.7-8.5-44.8c1.7-3.9,3.8-6.9,7.6-8.2c1.6-0.5,5.4-1.2,6.9-1.2 c3.4,0,6.4,4.4,8.5,7.1c2,2.5,6.5,7.9-2.5,45.9c9.1,18.8,22,37.9,34.4,51c8.9-1.6,16.5-2.4,22.7-2.4c10.6,0,17,2.5,19.6,7.6 c2.2,4.2,1.3,9.1-2.6,14.6c-3.8,5.3-9,8.1-15,8.1c-8.2,0-17.8-5.2-28.5-15.4c-19.1,4-41.5,11.1-59.6,19.1c-5.6,12-11,21.6-16.1,28.7 C135.2,306.9,129.2,311.4,123.3,311.4L123.3,311.4z M141.3,276.8c-14.4,8.1-20.3,14,8-20,7,18,5c-0.1,0.6-0.3,2. 2,2,9,4,7  C124,5,299.7,130.3,297,141.3, 276.8L141.3,276.8z M233.3,246.8c5.5,4.3,6.9,6.4, 10.5,6.4c1.6,0,6.1-0.1,8.2- 3c1-1.4, 1.4.2.3.1.6-2.8  c-0.8-0.4-1.9-1.3-7.9-1.3C242. 2,246,1,237.9,246.2.233.3246. 8L233.3,246.8z M182.9,202.4c-4.8,16.7-11.2, 34.7-18.51.1  c14.1-5.5,29.4-10.3,43.8-13. 6C199.6,229.2,190.5,216,182.9, 202.4L182.9,202.4z M178.8,145.2c-0.7,0.2-9,11.9, 0.7,21.7  C185.9,152.7,179.1.145.1,178. 8,145.2L178.8,145.2z M178.8,145.2 \"  /> 
									</svg>";
							echo "</div>";
							echo "<div class='conteudo'> 
									<h4>Balanço {$item['ano']}</h4> 
									<a href='".ROOT.$item['pdf']."' target='_blank' class='btn bg-blue'><i class='inlineSVG'>";
							//include(DIR."resources/img/view.svg");
							echo "</i>Ver online</a>
									<a href='".ROOT."institucional/portal-transparencia/file-{$item['id_balanco']}"."' class='btn bg-blue'><i class='inlineSVG'>";
							//include(DIR."resources/img/download.svg");
							echo "</i>Download</a>
								</div>
							</div>";

						}
					}
				?>
					

				<!-- 
								<figure class='img-pdf'>
								<img src='".ROOT."resources/img/pdf.png' alt='Balanço {$item['ano']} - Portal da Transparência' >
								<figcaption class='ano-pdf bg-blue'>{$item['ano']}</figcaption>
								</figure>
								-->

			</div>
			<div class='clearBoth'></div>
		</div>
		<div class='clearBoth'></div>
	</div>
	<div class='clearBoth'></div>
</section>
<div class='clearBoth'></div>