<div class='header-nav bg-white'>
	<div class='control'>
		<div class='to-left'>
			<a href='index.php'><h1 class='logo'>NEA CURSOS</h1></a>
			<button class='btn-responsive cl-white bg-gradient-blue hv-bg-gradient-blue-inverse'><i class="fas fa-bars"></i></button>
		</div>
		<nav class='to-right main-nav'>
			<a href='#' class='scrollactive cl-gray hv-cl-blue'>HOME</a>
			<a href='#quem-somos' class='scrollactive cl-gray hv-cl-blue'>QUEM SOMOS</a>
			<a href='#pos-graduacao' class='scrollactive cl-gray hv-cl-blue'>PÓS-GRADUAÇÃO</a>
			<a href='#especializacao' class='scrollactive cl-gray hv-cl-blue'>EXTENSÃO</a>
			<a href='#fotos' class='scrollactive cl-gray hv-cl-blue'>FOTOS</a>
			<a href='#contato' class='scrollactive cl-gray hv-cl-blue'>CONTATO</a>
			<a href='#inscricoes' class='scrollactive btn cl-white bg-gradient-blue hv-bg-gradient-blue-inverse'>INSCRIÇÕES</a>
		</nav>
	</div>
</div>