<section class='requerimento'>
  <div class='control bg-gray-light'>
    <div class='intern-control'>
      <div class='to-left'>
        <h2>Formulário de requerimento<span class='cl-blue'>.</span></h2>
        <!-- <h3>Início em 20/12/2017</h3> -->
        <hr class='marg bg-gradient-blue'><br>
        <p class='no-margin-bottom'>
          Para fazer seu requerimento de curso, basta preencher os dados abaixo e enviar para a nossa equipe. Depois disso, você receberá um e-mail de nossa equipe.
        </p>
      </div>
      <div class='clear-both'></div>
    </div>
  </div>
  <form class='control bg-white' method="post" id='form-requerimento' action="<?php echo HOME; ?>#quem-somos">
    
    <div class='intern-control'>

      <h4>Dados Pessoais<span class='cl-blue'>.</span></h4>

      <div class='half to-left'>

        <div class="form-group">
          <label>Nome Completo</label>
          <input type="text" name="nome_requerimento" class='input-style'>
          <label class='msg-erro'></label>
        </div>

        <div class="form-group">
          <label>Telefone</label>
          <input type="text" name="telefone_requerimento" class='input-style'>
          <label class='msg-erro'></label>
        </div>

      </div>

      <div class='half to-right'>

        <div class="form-group">
          <label>E-mail</label>
          <input type="text" name="email_requerimento" class='input-style'>
          <label class='msg-erro'></label>
        </div>

        <div class="form-group">
          <label>Celular</label>
          <input type="text" name="celular_requerimento" class='input-style'>
          <label class='msg-erro'></label>
        </div>

      </div>

      <div class="form-group">
        <div>
          Requerimento:
          <textarea id="msg" name="mensagem_requerimento" class='input-style' style='padding: 20px 0; height: 200px;'></textarea>
        </div>
        <label class='msg-erro'></label>
      </div>

      <hr class='clear-both small-full-hr bg-gray-light'>
    </div>

    <div class='intern-control' style="width: 100%;">
      <div class='half to-left'>
        <div class="g-recaptcha" data-sitekey="<?php echo $reCAPTCHA['client-key']; ?>"></div>
      </div>
    </div>

    <div class='control bg-white margin-top'>
      <button type='submit' class='form-btn sizable-btn bg-gradient-green hv-bg-gradient-green-inverse'><span class='cl-white'>Enviar Requerimento</span></button>
    </div>  

  </form>
  <button class='square-btn bg-gradient-blue hv-bg-gradient-blue-inverse'><span class='cl-white'><i class='fas fa-times'></i></span></button>
</section>

<script type="text/javascript">
  
  $("#form-requerimento").on('submit', function (e) {
        
    $(".form-group").find(".msg-erro").hide();
    e.preventDefault();

    if($("input[name='nome_requerimento']").val() == ""){
      $("input[name='nome_requerimento']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um nome válido");
      $("input[name='nome_requerimento']").focus();

    }else if($("input[name='email_requerimento']").val() == ""){
      $("input[name='email_requerimento']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um e-mail válido");
      $("input[name='email_requerimento']").focus();

    }else if($("input[name='telefone_requerimento']").val() == ""){
      $("input[name='telefone_requerimento']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um telefone válido");
      $("input[name='telefone_requerimento']").focus();

    }else if($("input[name='celular_requerimento']").val() == ""){
      $("input[name='celular_requerimento']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um celular válido");
      $("input[name='celular_requerimento']").focus();

    }else if($("textarea[name='mensagem_requerimento']").val() == ""){
      $("textarea[name='mensagem_requerimento']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira uma mensagem válida");
      $("textarea[name='mensagem_requerimento']").focus();

    }else{
      $(this).off('submit').submit();
    }
  });

</script>