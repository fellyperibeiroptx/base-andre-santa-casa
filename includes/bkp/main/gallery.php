<?php 
	
	$getGaleria = new Read();
	$getGaleria->fullRead("SELECT * FROM ".PREFIX."galeria WHERE status = 1");

	if($getGaleria->getResult()):
?>

<section id='fotos' class='bg-white'>
	<div class='control no-padding-left no-padding-right'>
		<h2>Galeria de fotos<span class='cl-blue'>.</span></h2>
		<h3>Confira nossa galeria de fotos, com nossos alunos e professores!</h3>
		<hr class='warp bg-gradient-blue'>
		<div class="owl-gallery popup-gallery owl-carousel">
		<?php
			foreach($getGaleria->getResult() AS $key => $galeria):
				echo "<div><a href='{$galeria['img']}' title='{$galeria['titulo']}'><img src='{$galeria['img']}'></a></div>";
			endforeach;
		?>
		</div>
		<div class='center'>
			<!-- <button class='normal-btn lightbox-gallery-btn bg-gradient-blue hv-bg-gradient-blue-inverse'><span class='cl-white'>Mais fotos</span></button> -->
		</div>
	</div>
</section>
<?php
	endif;
?>