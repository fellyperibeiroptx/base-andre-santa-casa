<!DOCTYPE html>
<html>
<head>
	<meta charset='utf-8'>
	<meta http-equiv='X-UA-Compatible' content='IE=edge'>
	<meta name='author' content=''>
	<meta name='viewport' content='width=device-width, initial-scale=1.0'>

	<title>Nea - Núcleo de estudos avançados</title>

	<meta name='keywords' content=''>
	<meta name='description' content=''>
	<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'>

	<link rel="apple-touch-icon" sizes="57x57" href="resources/img/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="resources/img/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="resources/img/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="resources/img/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="resources/img/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="resources/img/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="resources/img/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="resources/img/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="resources/img/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="resources/img/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="resources/img/ico.png">
	<link rel="icon" type="image/png" sizes="96x96" href="resources/img/ico.png">
	<link rel="icon" type="image/png" sizes="16x16" href="resources/img/ico.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<!-- fonts -->
	<script defer src='https://use.fontawesome.com/releases/v5.0.1/js/all.js'></script>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'>

	<!-- Owl-Carousel -->
	<link href='resources/plugins/owlcarousel/dist/assets/owl.carousel.min.css' rel='stylesheet'>
	<link href='resources/plugins/owlcarousel/dist/assets/owl.theme.default.min.css' rel='stylesheet'>
	<script src='resources/plugins/jQuery/jquery-3.2.1.min.js'></script>
	<script src='resources/plugins/owlcarousel/dist/owl.carousel.min.js'></script>

	<!-- Select -->
	<link rel="stylesheet" type="text/css" href="resources/plugins/selectJF/selectPlugin.css">
	<script type="text/javascript" src="resources/plugins/selectJF/selectPlugin.js"></script>

	<!-- css -->
	<link rel='stylesheet' type='text/css' href='resources/css/reset.css'>
	<link rel='stylesheet' type='text/css' href='resources/css/color.css'>
	<link rel='stylesheet' type='text/css' href='resources/css/style.css'>
	
	<!-- popup -->	
	<link rel="stylesheet" type="text/css" href="resources/plugins/magnific-popup-master/dist/magnific-popup.css">

	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>