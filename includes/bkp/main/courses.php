<section class='bg-gradient-blue'>
	<div class='control'>
<?php 
include_once('includes/courses-postgraduate.php');
?>
<hr class="megawarp">
<?php 
include_once('includes/courses-extentions.php');
?>
	</div>
</section>

<section class='course-list-pos'>
	<div class='control bg-gray-light'>
		<div class='intern-control'>
			<div class='to-left'>
				<h2>Cursos<span class='cl-blue'>.</span></h2>
				<h3></h3>
				<hr class='marg bg-gradient-blue'><br>
				<p class='no-margin-bottom'>
				</p>
			</div>
			<div class='clear-both'></div>
		</div>
	</div>
	<div class='control bg-white'>
		<div class='intern-control'>

			<?php 

				$getCursosPG = new Read();
				$getCursosPG->fullRead("SELECT C.*, DATE_FORMAT(T.data, '%d/%m/%Y') AS data_inicio
											FROM ".PREFIX."curso AS C
											LEFT JOIN ".PREFIX."turma AS T ON (T.id_curso = C.id_curso)
											WHERE C.tipo = 'Pós-Graduação' AND C.status = 1 GROUP BY C.id_curso");

				if($getCursosPG->getResult()):
					foreach ($getCursosPG->getResult() AS $key => $curso) {
			?>

					<div class='disciplina'>
						<div>
							<h4><?php echo $curso['titulo'] ?><span class='cl-blue'>.</span></h4>
							<?php echo $curso['descricao'] ?>
						</div>
						<span onClick="getCurso('<?php echo $curso['url_amigavel']; ?>')" class='bg-gradient-green cl-white'>Inscreva-se</span>
					</div>

			<?php 
					}
				endif;
			?>

		</div>
	</div>
	<button class='square-btn bg-gradient-blue hv-bg-gradient-blue-inverse'><span class='cl-white'><i class='fas fa-times'></i></span></button>
</section>

<section class='course-list-ext'>
	<div class='control bg-gray-light'>
		<div class='intern-control'>
			<div class='to-left'>
				<h2>Cursos<span class='cl-blue'>.</span></h2>
				<h3></h3>
				<hr class='marg bg-gradient-blue'><br>
				<p class='no-margin-bottom'>
				</p>
			</div>
			<div class='clear-both'></div>
		</div>
	</div>
	<div class='control bg-white'>
		<div class='intern-control'>

			<?php 

				$getCursosEx = new Read();
				$getCursosEx->fullRead("SELECT C.*, DATE_FORMAT(T.data, '%d/%m/%Y') AS data_inicio
											FROM ".PREFIX."curso AS C
											LEFT JOIN ".PREFIX."turma AS T ON (T.id_curso = C.id_curso)
											WHERE C.tipo = 'Extensão' AND C.status = 1 GROUP BY C.id_curso");

				if($getCursosEx->getResult()):
					foreach ($getCursosEx->getResult() AS $key => $curso) {
			?>

					<div class='disciplina'>
						<div>
							<h4><?php echo $curso['titulo'] ?><span class='cl-blue'>.</span></h4>
							<?php echo $curso['descricao'] ?>
						</div>
						<span onClick="getCurso('<?php echo $curso['url_amigavel']; ?>')" class='bg-gradient-green cl-white'>Inscreva-se</span>
					</div>

			<?php 
					}
				endif;
			?>

		</div>
	</div>
	<button class='square-btn bg-gradient-blue hv-bg-gradient-blue-inverse'><span class='cl-white'><i class='fas fa-times'></i></span></button>
</section>