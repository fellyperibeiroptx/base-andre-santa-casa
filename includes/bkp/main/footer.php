<footer class='bg-gradient-blue'>
	<div class='control'>
		<p class='cl-white' style='margin-bottom:20px'>
			© 2014 N.E.A Cursos Núcleo de Estudos Avançados.
			<br>
			Desenvolvido por <a href='http://www.pentaxial.com.br' class='cl-white hv-cl-green' target='_blank'>Pentaxial</a>. Todos os direitos reservados.
			<br>
			Proibida reprodução total ou parcial.
		</p>

		<div class='center'>
			<a href='https://www.facebook.com/neacursos/' target='_blank'>
				<div class=' social-icon social-icon-facebook bg-white radius cl-blue hv-cl-blue-dark'><i class='fab fa-facebook-f'></i></div>
			</a>
			<a href='https://www.instagram.com/neacursos/' target='_blank'>
				<div class=' social-icon social-icon-instagram bg-white radius cl-blue hv-cl-blue-dark'><i class='fab fa-instagram'></i></div>
			</a>
			<a href='https://www.youtube.com/channel/UCLt1XdduFlkzW14ZAvMsBnA' target='_blank'>
				<div class=' social-icon social-icon-youtube bg-white radius cl-blue hv-cl-blue-dark'><i class='fab fa-youtube'></i></div>
			</a>
			<a  href='http://api.whatsapp.com/send?1=pt_BR&phone=5512991215593' target='_blank'>
				<div style="color:#25d366" class=' social-icon social-icon-whatsapp bg-white radius cl-blue hv-cl-blue-dark'><i class='fab fa-whatsapp'></i></div>
			</a>
		</div>
	</div>
</footer>

<a href='#' class='back-to-top square-btn bg-gradient-blue hv-bg-gradient-blue-inverse'><span class='cl-white'><i class="fas fa-angle-up"></i></span></a>

<!-- javascript -->
	<!-- owl carousel -->
	<script src="resources/plugins/owlcarousel/docs_src/assets/vendors/highlight.js"></script> 
	<script src="resources/plugins/owlcarousel/docs/assets/js/app.js"></script>
	<script src="resources/js/carousel.js"></script>

	<!-- font awesome -->
	<script defer src='https://use.fontawesome.com/releases/v5.0.1/js/all.js'></script>

	<!-- other -->
	<script type="text/javascript" src='resources/plugins/magnific-popup-master/dist/jquery.magnific-popup.min.js'></script>
	<script src="resources/js/nav.js"></script>
	<script src="resources/js/scroll.js"></script>
	<!-- <script src="resources/js/scrollanimation.js"></script> -->
	<script src="resources/js/lightbox.js"></script>
	<script src="resources/js/info.js"></script>
	<script src="resources/js/form.js"></script>
</body>
</html>