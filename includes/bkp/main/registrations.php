<section id='inscricoes' class='registrations'>
	<div class='control'>
		<h2>Inscrições<span class='cl-blue'>.</span></h2>
		<h3>Preparação, qualificação, oportunidade, seu futuro profisional  começa agora!</h3>
		<hr class='warp bg-gradient-blue'>
		<p class='center'>O NEA cursos se reserva o direito de alterar a data do curso caso o número mínimo de alunos não seja atingido até uma semana antes do início do mesmo. O NEA não se responsabiliza por eventuais despesas para os alunos caso isso ocorra. No caso de cancelamento do curso, os valores pagos antecipadamente serão integralmente devolvidos.</p>
		


		<!-- carousel -->
		<div class='intern-control owl-carousel owl-courses-registrations padding-bottom'>

			<?php 

				$getInscricoesAbertas = new Read();
				$getInscricoesAbertas->fullRead("SELECT C.*, DATE_FORMAT(T.data, '%d/%m/%Y') AS data_inicio 
												FROM ".PREFIX."curso AS C
												INNER JOIN ".PREFIX."turma AS T ON (T.id_curso = C.id_curso)
												WHERE T.data >= NOW() AND C.status = 1 AND T.status = 1 ORDER BY T.data ASC");

				if($getInscricoesAbertas->getResult()):

					foreach ($getInscricoesAbertas->getResult() AS $key => $curso) {
						
						echo "<div>
							<a href='javascript:getInscricoes(\"{$curso['tipo']}\", \"{$curso['url_amigavel']}\")'>
								<div class='owl-course-item hv-bg-white cl-white hv-cl-blue'>
									<div class='control-image'>
										<img src='{$curso['img']}'>
										<div class='hover-button'>
											<button class='form-btn normal-btn bg-gradient-green hv-bg-gradient-green-inverse'><span class='cl-white'>Inscreva-se</span></button>
										</div>
									</div>
									<h3 class='cl-white'> {$curso['titulo']} </h3>
									<h4>Início: {$curso['data_inicio']}</h4>
								</div>
							</a>
						</div>";
					}

				endif;

			?>

		</div>
		<!-- end | carousel -->



		<div class='intern-control bg-white'>
			<div class='third-control padding-top padding-bottom'>
				<div class='to-left half no-padding'>
					<h2>Seu curso não estão com inscrições abertas<span class='cl-blue'>?</span></h2>
				</div>
				<div class='to-right half'>
					<button class='normal-btn normal-btn-100 bg-gradient-green hv-bg-gradient-green-inverse'><span class='cl-white' onClick='setRequirement()'>Clique aqui e faça o requerimento</span></button>
				</div>
				<div class='clear-both'></div>
			</div>
		</div>
	</div>
</section>