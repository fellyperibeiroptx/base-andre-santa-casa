<section id='contato' class='contact bg-white'>
	<div class='control'>
		<h2>Contato<span class='cl-blue'>.</span></h2>
		<h3>Tem alguma dúvida? Entre em contato conosco:</h3>
		<hr class='warp bg-gradient-blue'>
		<div class='intern-control'>

			<?php 

				if(isset($_POST['nome_contato'])){

					if($dados['g-recaptcha-response']){

						$recaptcha = $dados['g-recaptcha-response'];

				        $object = new Recaptcha($reCAPTCHA);
				        $response = $object->verifyResponse($recaptcha);

				        if(isset($response['success']) and $response['success'] != true) {

				        	echo "<div class='alert alert-warning alert-dismissible'>
					           	<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
					            <h4>Erro!</h4>
					            Captcha invalido
					        </div>";

				        }else{

							$contato = array(
							  	'nome' => $_POST['nome_contato'],
							  	'email' => $_POST['email_contato'],
							  	'telefone' => $_POST['telefone'],
							  	'tipo_contato' => $_POST['tipo_contato'],
							  	'mensagem' => $_POST['mensagem_contato'],
							  	'data_contato' => date("Y-m-d H:i:s")
							);

							$newContato = new Create();
							$newContato->ExeCreate(PREFIX."contato", $contato);
							if($newContato->getResult()){

								require_once('_app/PHPMailer-master/PHPMailer.class.php');
						        require_once('_app/PHPMailer-master/SMTP.class.php');

						        //unset($inscricao['captcha']);
						        $contato = array_map('strip_tags', $contato);
						        $contato = array_map('trim', $contato);
						                      
						        $nomeRemetente = $contato['nome'];

						        //$emaildestinatario1 = "andre@pentaxialroot.com.br";
						        $emaildestinatario1 = "contato@neacursos.com.br";

						                           
						        $Subject = "NEA Cursos - Contato";

						        $Message = nl2br("<font size='4' face='Arial' color='#3f3f3f'><p align='center'><b>Novo contato através do site</b></p></font>
						            <hr>
						            <font face='Arial' color='#3f3f3f'><b>Nome:</b></font> <font face='Arial' color='#999'>{$contato['nome']}</font><br>
						            <font face='Arial' color='#3f3f3f'><b>E-mail:</b></font> <font face='Arial' color='#999'>{$contato['email']}</font><br> 
						            <font face='Arial' color='#3f3f3f'><b>Telefone:</b></font> <font face='Arial' color='#999'>{$contato['telefone']}</font><br>
						            <font face='Arial' color='#3f3f3f'><b>Forma de contato:</b></font> <font face='Arial' color='#999'>{$contato['tipo_contato']}</font><br> 
						            <font face='Arial' color='#3f3f3f'><b>Mensagem:</b></font> <font face='Arial' color='#999'>{$contato['mensagem']}</font><br>  
						        ");
						                            
						        $body = utf8_decode($Message);
						        $email = new PHPMailer();

						        $emailremetente = 'webmaster@neacursos.com.br'; 
						        $from_name = $nomeRemetente;

						        $email = new PHPMailer();
						        $email->setLanguage('br');
						        //$email->isSMTP();
						        //$email->Host = 'smtp.villarreal.com.br';
						       	//$email->SMTPAuth = true;
						        //$email->Port = '587';
						        //$email->Username = 'webmaster@villarreal.com.br';
						        //$email->Password = '#zaraGoza2016';

						        $email->From = $emailremetente;
						        //$email->Sender = $emailremetente;
						        $email->FromName = utf8_decode($from_name);
						        //$email->ReturnPath = $emailremetente;
						        $email->addAddress($emaildestinatario1);
						        //$email->addAddress($emaildestinatario2);

						        $email->WordWrap = 50;
						        $email->isHTML(true);
						        $email->Subject = utf8_decode($Subject);
						        $email->Body  = $body;
						        $email->AltBody = 'Para visualizar este e-mail corretamente use o visualizador de e-mail com suporte a HTML';

						        echo "<div class='alert alert-success alert-dismissible'>
					            	<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
					                <h4>Sucesso!</h4>
					                Contato realizado com sucesso
					            </div>";

						        if($email->Send()){
						            /*  
						            echo "<div class='form-group'>
						                <label class='col-sm-12 control-label' style='color: #00a65a !important;'>E-mail enviado com sucesso</label>
						             </div>";
						            */
						        }else{
						            /*        
						            echo "<div class='form-group'>
						                <label class='col-sm-12 control-label' style='color: #f39c12 !important;'>Erro ao enviar e-mail</label>
						            </div>";
						            */
						        }

							}else{

								echo "<div class='alert alert-warning alert-dismissible'>
					            	<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
					                <h4>Erro!</h4>
					                Erro ao realizar inscrição
					            </div>";

							}
						}
					}
				}
			?>

			<div class='third-control margin-bottom'>
				<p class='center no-margin-bottom'>Para entrar em contato conosco é muito fácil. Preencha corretamente o formulário abaixo que em breve entraremos em contato com você. Mas se preferir, ligue ou venha até nós.</p>
			</div>

			<form class='third-control margin-bottom' method="post" id='form-contato' action="<?php echo HOME; ?>#contato">

				<div class="form-group">
				    <div>
				        Nome:
				        <input type="text" id="nome" name="nome_contato" class='input-style'/>
				    </div>
				    <label class='msg-erro'></label>
				</div>

				<div class="form-group">
				    <div>
				        E-mail:
				        <input type="email" id="email" name="email_contato" class='input-style'/>
				    </div>
				    <label class='msg-erro'></label>
				</div>

				<div class="form-group">
				    <div>
				        Telefone:
				        <input type="telefone" id="telefone" name="email_telefone" class='input-style'/>
				    </div>
				    <label class='msg-erro'></label>
				</div>

				<div class="form-group">
				    <div>
				        Forma de contato:
				        <select id="tipo_contato" name="tipo_contato" class='input-style'>
				        	<option value="">Selecine a forma de contato</option>
				        	<option value="E-mail">E-mail</option>
				        	<option value="Telefone">Telefone</option>
				        	<option value="Whatsapp">Whatsapp</option>
				        </select>
				    </div>
				    <label class='msg-erro'></label>
				</div>

				<div class="form-group">
				    <div>
				        Mensagem:
				        <textarea id="msg" name="mensagem_contato" class='input-style' style='padding: 20px 0; height: 200px;'></textarea>
				    </div>
				    <label class='msg-erro'></label>
				</div>

				<div class='intern-control' style="width: 100%;">
					<div class='half to-left'>
						<div class="g-recaptcha" data-sitekey="<?php echo $reCAPTCHA['client-key']; ?>"></div>
					</div>
				</div>

			    <div class='center'>	
			    	<button type='submit' class='form-btn sizable-btn bg-gradient-green hv-bg-gradient-green-inverse'><span class='cl-white'>Enviar</span></button>
				</div>

			</form>

			<hr class='margin-bottom small-full-hr bg-gray-light'>
			<div class='to-left half no-padding'>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.9125751257634!2d-45.57083568503236!3d-23.026981984950933!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ccf8f923a67b9f%3A0xab5f6ed167116e19!2sR.+Benjamin+Constant%2C+70+-+Jardim+das+Na%C3%A7%C3%B5es%2C+Taubat%C3%A9+-+SP%2C+12030-170!5e0!3m2!1spt-BR!2sbr!4v1513596067899" width="100%" height="220" frameborder="0" style="border:0" allowfullscreen></iframe>
				
				<div class='padding-top'>
					<h4>Unidade Taubaté<spam class='cl-blue'>.</spam></h4>
					<p>
						Colégio Jardim das Nações, Portão 03
						<br>
						CEP 12030-170
						<br>
						Rua Benjamin Constant, 70
						<br>
						Jardim das Nações, Taubaté-SP
					</p>
					<h5>Telefone<spam class='cl-blue'>.</spam></h5>
					<p>
						(12) 3621-8408
						<br>
						(12) 9 9121-5593 (Whatsapp)
		 			</p>
					<h5>E-mail<spam class='cl-blue'>.</spam></h5>
					<p>contato@neacursos.com.br</p>
				</div>

			</div>
			<div class='to-right half no-padding'>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3668.3575430214987!2d-45.79411283451061!3d-23.15714715267523!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cc4c345d24cd69%3A0x3baf325177feb8a0!2sParque+Tecnol%C3%B3gico+S%C3%A3o+Jos%C3%A9+dos+Campos!5e0!3m2!1spt-BR!2sbr!4v1513596201699" width="100%" height="220" frameborder="0" style="border:0" allowfullscreen></iframe>
				<div class='padding-top'>
					<h4>Unidade de São José dos Campos<spam class='cl-blue'>.</spam></h4>
					<p>
						Parque Tecnológico de São José dos Campos
						<br>
						CEP 12247-016
						<br>
						Av. Doutor Altino Bondensan, 500
						<br>
						Distrito Eugênio de Melo, São José dos Campos-SP
					</p>
					<h5>Telefone<spam class='cl-blue'>.</spam></h5>
					<p>(12) 9 9121-5593 (Whatsapp)</p>
				</div>

				<br><br>

				<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3667.318161374184!2d-45.901434685028974!3d-23.19507298486591!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cc4a0b76d536e9%3A0x13f9c507cbb37ed3!2sAv.+Bar%C3%A3o+do+Rio+Branco%2C+48+-+Jardim+Esplanada%2C+S%C3%A3o+Jos%C3%A9+dos+Campos+-+SP%2C+12242-800!5e0!3m2!1spt-BR!2sbr!4v1516986447970" width="100%" height="220" frameborder="0" style="border:0" allowfullscreen></iframe> -->
				<div class='padding-top'>

					<h4>Clínica Byc<spam class='cl-blue'>.</spam></h4>
					<p>
						São José dos Campos
						<br>
						CEP 12220-000
						<br>
						Av. Barão do Rio Branco, 48
						<br>
						Jardim Esplanada, São José dos Campos - SP
					</p>
				</div>

			</div>
			<div class='clear-both'></div>
		</div>
	</div>
</section>

<script type="text/javascript">
	
	$("#form-contato").on('submit', function (e) {
        
        $(".form-group").find(".msg-erro").hide();
        e.preventDefault();

        if($("input[name='nome_contato']").val() == ""){
        	$("input[name='nome_contato']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um nome válido");
        	$("input[name='nome_contato']").focus();

        }else if($("input[name='email_contato']").val() == ""){
        	$("input[name='email_contato']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um e-mail válido");
        	$("input[name='email_contato']").focus();

        }else if($("textarea[name='telefone']").val() == ""){
        	$("textarea[name='telefone']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um telefone válida");
        	$("textarea[name='telefone']").focus();

        }else if($("input[name='tipo_contato']").val() == ""){
        	$("input[name='tipo_contato']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um meio de contato válido");
        	$("input[name='tipo_contato']").focus();

        }else if($("textarea[name='mensagem_contato']").val() == ""){
        	$("textarea[name='mensagem_contato']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira uma mensagem válida");
        	$("textarea[name='mensagem_contato']").focus();

        }else{
            $(this).off('submit').submit();
        }
    });

</script>