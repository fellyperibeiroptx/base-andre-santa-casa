<section class='info'>
	<div class='control bg-gray-light'>
		<div class='intern-control'>
			<div class='to-left'>
				<h2 id='curso-titulo'></h2>
				<h3 id='curso-data'></h3>
				<hr class='marg bg-gradient-blue'><br>
				<p class='no-margin-bottom' id='curso-local'>
				</p>
			</div>
			<div class='to-right'>
				<button class='form-btn sec-normal-btn margin-top bg-gradient-green hv-bg-gradient-green-inverse' id='btn-insc-top'><span class='cl-white'>Inscreva-se</span></button>
			</div>
			<div class='clear-both'></div>
		</div>
	</div>
	<div class='control bg-white'>
		<div class='intern-control'>
			<div class='to-left lateral-info margin-bottom bg-gray-light'>
				<h4>Carga horária<span class='cl-blue'>.</span></h4>
				<p id='curso-carga_horaria'>
					
				</p>
				<hr class='small-full-hr bg-whi bg-white'>
				<h4>Conteúdo teórico<span class='cl-blue'>.</span></h4>
				<p id='curso-conteudo_teorico'></p>
				<hr class='small-full-hr bg-white'>
				<h4>Pré-requisitos<span class='cl-blue'>.</span></h4>
				<p id='curso-pre_requisitos'></p>
			</div>
			<div class='to-right no-lateral-info'>
				<h4>Sobre o curso<span class='cl-blue'>.</span></h4>
				<p id='curso-descricao'></p>

				<h4 class='margin-top'>Corpo docente<span class='cl-blue'>.</span></h4>
				<div id='curso-corpo_docente'></div>

				<h4 class='margin-top'>Coordenação<span class='cl-blue'>.</span></h4>
				<div id='curso-coordenacao'></div>

			</div>
			<div class='clear-both'></div>
		</div>
	</div>
	<div class='control bg-gray-light matriz-bloco'>
		<h2 class='margin-bottom '>Matriz Curricular<span class='cl-blue'>.</span></h2>
		
		<div id="matriz_curricular">

		</div>
		
	</div>
	<div class='control bg-white'>
		<h2 class='margin-bottom'>Garanta sua vaga<span class='cl-blue'>.</span></h2>
		<button class='form-btn sizable-btn bg-gradient-green hv-bg-gradient-green-inverse' id='btn-insc-bot'><span class='cl-white'>Faça sua inscrição aqui</span></button>
		ou
		<a href='#contato' onClick='limpaTela()' class='form-btn sizable-btn bg-gradient-green hv-bg-gradient-green-inverse' id='btn-insc-bot'><span class='cl-white'>Entre em contato</span></a>
	</div>	
	<button class='square-btn bg-gradient-blue hv-bg-gradient-blue-inverse'><span class='cl-white'><i class='fas fa-times'></i></span></button>
</section>