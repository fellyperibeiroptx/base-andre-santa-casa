<section class='formulario'>
	<div class='control bg-gray-light'>
		<div class='intern-control'>
			<div class='to-left'>
				<h2>Formulário de Inscrição<span class='cl-blue'>.</span></h2>
				<!-- <h3>Início em 20/12/2017</h3> -->
				<hr class='marg bg-gradient-blue'><br>
				<p class='no-margin-bottom'>
					Para fazer sua inscrição no curso escolhido, basta preencher os dados abaixo e enviar para a nossa equipe. Depois disso, você receberá um e-mail de nossa equipe com as instruções de pagamento e efetivação da inscrição.
				</p>
			</div>
			<div class='clear-both'></div>
		</div>
	</div>
	<form class='control bg-white' method="post" id='form-inscricao' action="<?php echo HOME; ?>#quem-somos">
		<div class='intern-control'>
			<h4>Dados do Curso<span class='cl-blue'>.</span></h4>
			<div class='half to-left'>

				<div class="form-group">
					<label>Tipo de Curso<span style="color:red;">*</span></label>
					<select id='tipo_curso' name='tipoDeCurso' class='select-form' onChange='getCursosPorTipo()'>
						<option value='default' selected="true">Selecione</option>
						<option value='Pós-Graduação'>Pós-Graduação</option>
						<option value='Extensão'>Extensão</option>
					</select>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">
					<label>Unidade de Ensino</label>
					<div class='radio'>
						<input type="radio" name="unidadeDeEnsino" id="taubate" value="Taubaté"><label for="taubate">Taubaté</label>
						<input type="radio" name="unidadeDeEnsino" id="saoJose" value="São José dos Campos"><label for="saoJose">São José</label>
					</div>
					<label class='msg-erro'></label>
				</div>

			</div>
			<div class='half to-right'>

				<div class="form-group">
					<label>Curso<span style="color:red;">*</span></label>
					<select id='curso-tipo' name='curso' class='select-form'>
						<option value='default' selected="true">Selecione o tipo de curso</option>
					</select>
					<label class='msg-erro'></label>
				</div>

			</div>
			<hr class='clear-both small-full-hr bg-gray-light'>
		</div>
		<div class='intern-control'>

			<h4>Dados do Pessoais<span class='cl-blue'>.</span></h4>

			<div class='half to-left'>

				<div class="form-group">
					<label>Nome Completo</label>
					<input type="text" name="nomeCompleto" class='input-style'>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">
					<label>RG</label>
					<input type="text" name="RG" class='input-style'>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">
					<label>Órgão Emissor</label>
					<input type="text" name="orgaoEmissor" class='input-style'>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">
					<label>Data de Nascimento</label>
					<input type="text" name="dataDeNascimento" class='input-style'>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">
					<label>Estado Civil</label>
					<select name='estadoCivil' class='select-form'>
						<option value='default' selected="true">Selecione</option>
						<option value='solteiro'>Solteiro</option>
						<option value='casado'>Casado</option>
					</select>
					<label class='msg-erro'></label>
				</div>

			</div>

			<div class='half to-right'>

				<div class="form-group">
					<label>CPF</label>
					<input type="text" name="cpf" class='input-style'>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">
					<label>Data Emissão</label>
					<input type="text" name="dataEmissao" class='input-style'>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">
					<label>Estado Emissão</label>
					<input type="text" name="estadoEmissao" class='input-style'>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">	
					<label>Sexo</label>
					<select name='sexo' class='select-form'>
						<option value='default' selected="true">Selecione</option>
						<option value='feminino'>Feminino</option>
						<option value='masculino'>Masculino</option>
					</select>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">
					<label>Naturalidade</label>
					<input type="text" name="naturalidade" class='input-style'>
					<label class='msg-erro'></label>
				</div>

			</div>

			<hr class='clear-both small-full-hr bg-gray-light'>
		</div>

		<div class='intern-control'>

			<h4>Dados para Contato<span class='cl-blue'>.</span></h4>

			<div class='half to-left'>

				<div class="form-group">
					<label>Telefone</label>
					<input type="text" name="telefone" class='input-style'>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">
					<label>Telefone Comercial</label>
					<input type="text" name="telefoneComercial" class='input-style'>
					<label class='msg-erro'></label>
				</div>

			</div>

			<div class='half to-right'>

				<div class="form-group">
					<label>Celular</label>
					<input type="text" name="celular" class='input-style'>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">
					<label>E-mail</label>
					<input type="text" name="email" class='input-style'>
					<label class='msg-erro'></label>
				</div>

			</div>

			<hr class='clear-both small-full-hr bg-gray-light'>
		</div>

		<div class='intern-control'>
			<h4>Endereço<span class='cl-blue'>.</span></h4>

			<div class='half to-right'>

				<div class="form-group">
					<label>Endereço</label>
					<input type="text" name="endereco" class='input-style'>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">
					<label>Bairro/Setor</label>
					<input type="text" name="bairro" class='input-style'>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">
					<label>Estado</label>
					<input type="text" name="estado" class='input-style'>
					<label class='msg-erro'></label>
				</div>

			</div>

			<div class='half to-left'>

				<div class="form-group">
					<label>CEP</label>
					<input type="text" name="cep" class='input-style'>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">
					<label>Número</label>
					<input type="text" name="numero" class='input-style'>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">
					<label>Cidade</label>
					<input type="text" name="cidade" class='input-style'>
					<label class='msg-erro'></label>
				</div>

			</div>
			<hr class='clear-both small-full-hr bg-gray-light'>
		</div>

		<div class='intern-control'>

			<h4>Formação Acadêmica<span class='cl-blue'>.</span></h4>

			<div class='half to-left'>

				<div class="form-group">
					<label>Curso</label>
					<input type="text" name="cursoFormacao" class='input-style'>
					<label class='msg-erro'></label>
				</div>

				<div class="form-group">
					<label>Conclusão</label>
					<input type="text" name="conclusaoFormacao" class='input-style'>
					<label class='msg-erro'></label>
				</div>

			</div>

			<div class='half to-right'>

				<div class="form-group">
					<label>Instituição</label>
					<input type="text" name="instituicaoFormacao" class='input-style'>
					<label class='msg-erro'></label>
				</div>

			</div>

			<hr class='clear-both small-full-hr bg-gray-light'>
		</div>

		<div class='intern-control'>

			<h4>Forma de Pagamento<span class='cl-blue'>.</span></h4>

			<div class='half to-left'>

				<div class="form-group">
					<select name='formaPagamento' class='select-form'>
						<option value='default' selected="true">Selecione</option>
						<option value='Boleto'>Boleto</option>
						<option value='Cheque'>Cheque</option>
						<option value='Dinheiro'>Dinheiro</option>
					</select>
					<label class='msg-erro'></label>
				</div>

			</div>

			<div class='half to-right'>

				<div class="form-group">
					<select name='pagamento' class='select-form'>
						<option value='default' selected="true">Selecione</option>
						<option value='A vista'>A vista</option>
						<option value='Parcelado'>Parcelado</option>
					</select>
					<label class='msg-erro'></label>
				</div>

			</div>

			<div class="clear-both"></div>
		</div>

		<div class='intern-control' style="width: 100%;">
			<div class='half to-left'>
				<div class="g-recaptcha" data-sitekey="<?php echo $reCAPTCHA['client-key']; ?>"></div>
			</div>
		</div>

		<div class='control bg-white margin-top'>
			<button type='submit' class='form-btn sizable-btn bg-gradient-green hv-bg-gradient-green-inverse'><span class='cl-white'>Enviar Formulário</span></button>
		</div>	

	</form>
	<button class='square-btn bg-gradient-blue hv-bg-gradient-blue-inverse'><span class='cl-white'><i class='fas fa-times'></i></span></button>
</section>
<!--
	tipoDeCurso
	unidadeDeEnsino
  	curso
  	nomeCompleto
  	RG
  	orgaoEmissor
  	dataDeNascimento
  	estadoCivil
  	cpf
  	dataEmissao
  	estadoEmissao
  	sexo
  	naturalidade
  	telefone
  	telefoneComercial
  	celular
  	email
  	endereco
  	bairro
  	estado
  	cep
  	numero
  	cidade
  	cursoFormacao
  	conclusaoFormacao
  	instituicaoFormacao
  	formaPagamento
  	pagamento
-->

<script type="text/javascript">
	
	$("#form-inscricao").on('submit', function (e) {
        
        $(".form-group").find(".msg-erro").hide();
        e.preventDefault();
    	
        //$(".form-group").removeClass("has-warning").find(".msg-erro").fadeOut().text("");

        if($("select[name='tipoDeCurso']").val() == ""){
        	$("select[name='tipoDeCurso']").closest(".form-group").find(".msg-erro").fadeIn().text("Selecione o tipo de curso");
        	$("select[name='tipoDeCurso']").focus();

        }else if($("input[name='unidadeDeEnsino']:checked").length == 0){
        	$("input[name='unidadeDeEnsino']").closest(".form-group").find(".msg-erro").fadeIn().text("Selecione uma unidade de ensino");
        	$("input[name='unidadeDeEnsino']").focus();

        }else if($("select[name='curso']").val() == ""){
        	$("select[name='curso']").closest(".form-group").find(".msg-erro").fadeIn().text("Selecione um curso");
        	$("select[name='curso']").focus();

        }else if($("input[name='nomeCompleto']").val() == ""){
        	$("input[name='nomeCompleto']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um nome válido");
        	$("input[name='nomeCompleto']").focus();


        }else if($("input[name='RG']").val() == "" && $("select[name='tipoDeCurso']").val() == "Pós-Graduação"){
        	$("input[name='RG']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um RG válido");
        	$("input[name='RG']").focus();

        }else if($("input[name='orgaoEmissor']").val() == "" && $("select[name='tipoDeCurso']").val() == "Pós-Graduação"){
        	$("input[name='orgaoEmissor']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira o orgão emissor");
        	$("input[name='orgaoEmissor']").focus();

        }else if($("input[name='dataDeNascimento']").val() == "" && $("select[name='tipoDeCurso']").val() == "Pós-Graduação"){
        	$("input[name='dataDeNascimento']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira uma data válida");
        	$("input[name='dataDeNascimento']").focus();

        }else if($("select[name='estadoCivil']").val() == ""){
        	$("select[name='estadoCivil']").closest(".form-group").find(".msg-erro").fadeIn().text("Selecione o estado civil");
        	$("select[name='estadoCivil']").focus();

        }else if($("input[name='cpf']").val() == ""){
        	$("input[name='cpf']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um CPF válido");
        	$("input[name='cpf']").focus();

        }else if($("input[name='dataEmissao']").val() == "" && $("select[name='tipoDeCurso']").val() == "Pós-Graduação"){
        	$("input[name='dataEmissao']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira uma data válida");
        	$("input[name='dataEmissao']").focus();

        }else if($("select[name='estadoEmissao']").val() == "" && $("select[name='tipoDeCurso']").val() == "Pós-Graduação"){
        	$("select[name='estadoEmissao']").closest(".form-group").find(".msg-erro").fadeIn().text("Selecione o estado");
        	$("select[name='estadoEmissao']").focus();

        }else if($("select[name='sexo']").val() == "" && $("select[name='tipoDeCurso']").val() == "Pós-Graduação"){
        	$("select[name='sexo']").closest(".form-group").find(".msg-erro").fadeIn().text("Selecione um sexo");
        	$("select[name='sexo']").focus();

        }else if($("input[name='naturalidade']").val() == "" && $("select[name='tipoDeCurso']").val() == "Pós-Graduação"){
        	$("input[name='naturalidade']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira sua naturalidade");
        	$("input[name='naturalidade']").focus();

        }else if($("input[name='telefone']").val() == "" && $("select[name='tipoDeCurso']").val() == "Pós-Graduação"){
        	$("input[name='telefone']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um telefone válido");
        	$("input[name='telefone']").focus();

        }else if($("input[name='celular']").val() == ""){
        	$("input[name='celular']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um celular válido");
        	$("input[name='celular']").focus();

        }else if($("input[name='email']").val() == ""){
        	$("input[name='email']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um email válido");
        	$("input[name='email']").focus();

        }else if($("input[name='endereco']").val() == ""){
        	$("input[name='endereco']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um endereço válido");
        	$("input[name='endereco']").focus();

        }else if($("input[name='bairro']").val() == ""){
        	$("input[name='bairro']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um bairro válido");
        	$("input[name='bairro']").focus();

        }else if($("select[name='estado']").val() == ""){
        	$("select[name='estado']").closest(".form-group").find(".msg-erro").fadeIn().text("Selecione o estado");
        	$("select[name='estado']").focus();

        }else if($("input[name='cep']").val() == ""){
        	$("input[name='cep']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um CEP");
        	$("input[name='cep']").focus();

        }else if($("input[name='numero']").val() == ""){
        	$("input[name='numero']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira um número");
        	$("input[name='numero']").focus();

        }else if($("input[name='cidade']").val() == ""){
        	$("input[name='cidade']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira uma cidade válida");
        	$("input[name='cidade']").focus();

        }else if($("input[name='cursoFormacao']").val() == ""){
        	$("input[name='cursoFormacao']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira o curso de formação");
        	$("input[name='cursoFormacao']").focus();

        }else if($("input[name='conclusaoFormacao']").val() == ""){
        	$("input[name='conclusaoFormacao']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira uma data válida");
        	$("input[name='conclusaoFormacao']").focus();

        }else if($("input[name='instituicaoFormacao']").val() == ""){
        	$("input[name='instituicaoFormacao']").closest(".form-group").find(".msg-erro").fadeIn().text("Insira uma instituição");
        	$("input[name='instituicaoFormacao']").focus();

        }else if($("select[name='formaPagamento']").val() == ""){
        	$("select[name='formaPagamento']").closest(".form-group").find(".msg-erro").fadeIn().text("Selecione uma forma de pagemento");
        	$("select[name='formaPagamento']").focus();

        }else if($("select[name='pagamento']").val() == ""){
        	$("select[name='pagamento']").closest(".form-group").find(".msg-erro").fadeIn().text("Selecione o pagamento");
        	$("select[name='pagamento']").focus();

        }else{
            
            $(this).off('submit').submit();
        }
    });

</script>