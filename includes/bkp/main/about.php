<section id='quem-somos' class='bg-white'>

	<?php

		if(isset($_POST['unidadeDeEnsino'])){

			if($dados['g-recaptcha-response']){

				$recaptcha = $dados['g-recaptcha-response'];

		        $object = new Recaptcha($reCAPTCHA);
		        $response = $object->verifyResponse($recaptcha);

		        if(isset($response['success']) and $response['success'] != true) {

		        	echo "<div class='alert alert-warning alert-dismissible'>
		            	<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
		                <h4>Erro!</h4>
		                Captcha inválido. Preencha corretamente.
		            </div>";

		        }else{

					$inscricao = array(
						'tipo_curso' => $_POST['tipoDeCurso'],
						'unidade_ensino' => $_POST['unidadeDeEnsino'],
					  	'curso' => $_POST['curso'],
					  	'nome' => $_POST['nomeCompleto'],
					  	'rg' => $_POST['RG'],
					  	'orgao_emissor' => $_POST['orgaoEmissor'],
					  	'data_nascimento' => Check::Data($_POST['dataDeNascimento']),
					  	'estado_civil' => $_POST['estadoCivil'],
					  	'cpf' => $_POST['cpf'],
					  	'data_emissao' => Check::Data($_POST['dataEmissao']),
					  	'estado_emissao' => $_POST['estadoEmissao'],
					  	'sexo' => $_POST['sexo'],
					  	'naturalidade' => $_POST['naturalidade'],
					  	'telefone' => $_POST['telefone'],
					  	'telefone_comercial' => $_POST['telefoneComercial'],
					  	'celular' => $_POST['celular'],
					  	'email' => $_POST['email'],
					  	'endereco' => $_POST['endereco'],
					  	'bairro' => $_POST['bairro'],
					  	'estado' => $_POST['estado'],
					  	'cep' => $_POST['cep'],
					  	'numero' => $_POST['numero'],
					  	'cidade' => $_POST['cidade'],
					  	'curso_formacao' => $_POST['cursoFormacao'],
					  	'data_formacao' => Check::Data($_POST['conclusaoFormacao']),
					  	'instituicao_formacao' => $_POST['instituicaoFormacao'],
					  	'forma_pagamento' => $_POST['formaPagamento'],
					  	'pagamento' => $_POST['pagamento'],
					  	'data_inscricao' => date("Y-m-d H:i:s")
					);

					$newInscricao = new Create();
					$newInscricao->ExeCreate(PREFIX."inscricoes", $inscricao);
					if($newInscricao->getResult()){

						require_once('_app/PHPMailer-master/PHPMailer.class.php');
				        require_once('_app/PHPMailer-master/SMTP.class.php');

				        //unset($inscricao['captcha']);
				        $inscricao = array_map('strip_tags', $inscricao);
				        $inscricao = array_map('trim', $inscricao);
				                      
				        $nomeRemetente = $inscricao['nome'];

				        //$emaildestinatario1 = "andre@pentaxialroot.com.br";
				        $emaildestinatario1 = "contato@neacursos.com.br";

				                           
				        $Subject = "NEA Cursos - Inscrição";

				        $Message = nl2br("<font size='4' face='Arial' color='#3f3f3f'><p align='center'><b>Nova inscrição através do site</b></p></font>
				            <hr>
				            <font face='Arial' color='#3f3f3f'><b>Tipo de curso:</b></font> <font face='Arial' color='#999'>{$inscricao['tipo_curso']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Unidade de Ensino:</b></font> <font face='Arial' color='#999'>{$inscricao['unidade_ensino']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Curso:</b></font> <font face='Arial' color='#999'>{$inscricao['curso']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Nome:</b></font> <font face='Arial' color='#999'>{$inscricao['nome']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>RG:</b></font> <font face='Arial' color='#999'>{$inscricao['rg']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Orgão Emissor:</b></font> <font face='Arial' color='#999'>{$inscricao['orgao_emissor']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Data de Nascimento:</b></font> <font face='Arial' color='#999'>{$inscricao['data_nascimento']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Estado Civil:</b></font> <font face='Arial' color='#999'>{$inscricao['estado_civil']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>CPF:</b></font> <font face='Arial' color='#999'>{$inscricao['cpf']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Data Emissão:</b></font> <font face='Arial' color='#999'>{$inscricao['data_emissao']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Estado Emissão:</b></font> <font face='Arial' color='#999'>{$inscricao['estado_emissao']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Sexo:</b></font> <font face='Arial' color='#999'>{$inscricao['sexo']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Naturalidade:</b></font> <font face='Arial' color='#999'>{$inscricao['naturalidade']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Telefone:</b></font> <font face='Arial' color='#999'>{$inscricao['telefone']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Telefone Comercial:</b></font> <font face='Arial' color='#999'>{$inscricao['telefone_comercial']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Celular:</b></font> <font face='Arial' color='#999'>{$inscricao['celular']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>E-mail:</b></font> <font face='Arial' color='#999'>{$inscricao['email']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Endereço:</b></font> <font face='Arial' color='#999'>{$inscricao['endereco']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Bairro:</b></font> <font face='Arial' color='#999'>{$inscricao['bairro']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Estado:</b></font> <font face='Arial' color='#999'>{$inscricao['estado']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>CEP:</b></font> <font face='Arial' color='#999'>{$inscricao['cep']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Número:</b></font> <font face='Arial' color='#999'>{$inscricao['numero']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Cidade:</b></font> <font face='Arial' color='#999'>{$inscricao['cidade']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Curso de formação:</b></font> <font face='Arial' color='#999'>{$inscricao['curso_formacao']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Data de conclusão:</b></font> <font face='Arial' color='#999'>{$inscricao['data_formacao']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Instituição:</b></font> <font face='Arial' color='#999'>{$inscricao['instituicao_formacao']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Forma pagamento:</b></font> <font face='Arial' color='#999'>{$inscricao['forma_pagamento']}</font><br> 
				            <font face='Arial' color='#3f3f3f'><b>Pagamento:</b></font> <font face='Arial' color='#999'>{$inscricao['pagamento']}</font><br> 
				        ");
				                            
				        $body = utf8_decode($Message);
				        $email = new PHPMailer();

				        $emailremetente = 'webmaster@neacursos.com.br'; 
				        $from_name = $nomeRemetente;

				        $email = new PHPMailer();
				        $email->setLanguage('br');
				        //$email->isSMTP();
				        //$email->Host = 'smtp.villarreal.com.br';
				       	//$email->SMTPAuth = true;
				        //$email->Port = '587';
				        //$email->Username = 'webmaster@villarreal.com.br';
				        //$email->Password = '#zaraGoza2016';

				        $email->From = $emailremetente;
				        //$email->Sender = $emailremetente;
				        $email->FromName = utf8_decode($from_name);
				        //$email->ReturnPath = $emailremetente;
				        $email->addAddress($emaildestinatario1);
				        //$email->addAddress($emaildestinatario2);

				        $email->WordWrap = 50;
				        $email->isHTML(true);
				        $email->Subject = utf8_decode($Subject);
				        $email->Body  = $body;
				        $email->AltBody = 'Para visualizar este e-mail corretamente use o visualizador de e-mail com suporte a HTML';

				       	echo "<div class='alert alert-success alert-dismissible'>
			            	<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
			                <h4>Sucesso</h4>
			                Inscrição realizada com sucesso
			            </div>";

				        if($email->Send()){
				            /*  
				            echo "<div class='form-group'>
				                <label class='col-sm-12 control-label' style='color: #00a65a !important;'>E-mail enviado com sucesso</label>
				             </div>";
				            */
				        }else{
				            /*        
				            echo "<div class='form-group'>
				                <label class='col-sm-12 control-label' style='color: #f39c12 !important;'>Erro ao enviar e-mail</label>
				            </div>";
				            */
				        }

					}else{

						echo "<div class='alert alert-warning alert-dismissible'>
			            	<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
			                <h4>Erro</h4>
			                Erro ao realizar inscrição
			            </div>";

					}
				}
			}
		}



		if(isset($_POST['nome_requerimento'])){

			if($dados['g-recaptcha-response'] || 1 == 1){

				$recaptcha = $dados['g-recaptcha-response'];

				$object = new Recaptcha($reCAPTCHA);
				$response = $object->verifyResponse($recaptcha);

				if(isset($response['success']) && $response['success'] != true) {

				    echo "<div class='alert alert-warning alert-dismissible'>
		            	<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
		                <h4>Erro!</h4>
		                Captcha inválido. Preencha corretamente.
		            </div>";

				}else{

					$requerimento = array(
						'nome' => $_POST['nome_requerimento'],
						'email' => $_POST['email_requerimento'],
						'telefone' => $_POST['telefone_requerimento'],
						'celular' => $_POST['celular_requerimento'],
						'mensagem' => $_POST['mensagem_requerimento'],
						'data_requerimento' => date("Y-m-d H:i:s")
					);

					$newRequerimento = new Create();
					$newRequerimento->ExeCreate(PREFIX."requerimento", $requerimento);
					if($newRequerimento->getResult()){

								require_once('_app/PHPMailer-master/PHPMailer.class.php');
						        require_once('_app/PHPMailer-master/SMTP.class.php');

						        //unset($inscricao['captcha']);
						        $requerimento = array_map('strip_tags', $requerimento);
						        $requerimento = array_map('trim', $requerimento);
						                      
						        $nomeRemetente = $requerimento['nome'];

						        //$emaildestinatario1 = "andre@pentaxialroot.com.br";
						        $emaildestinatario1 = "contato@neacursos.com.br";

						                           
						        $Subject = "NEA Cursos - Requerimento";

						        $Message = nl2br("<font size='4' face='Arial' color='#3f3f3f'><p align='center'><b>Novo requerimento através do site</b></p></font>
						            <hr>

						            <font face='Arial' color='#3f3f3f'><b>Nome:</b></font> <font face='Arial' color='#999'>{$requerimento['nome']}</font><br>
						            <font face='Arial' color='#3f3f3f'><b>E-mail:</b></font> <font face='Arial' color='#999'>{$requerimento['email']}</font><br> 
						            <font face='Arial' color='#3f3f3f'><b>Mensagem:</b></font> <font face='Arial' color='#999'>{$requerimento['mensagem']}</font><br>  
						        ");
						                            
						        $body = utf8_decode($Message);
						        $email = new PHPMailer();

						        $emailremetente = 'webmaster@neacursos.com.br'; 
						        $from_name = $nomeRemetente;

						        $email = new PHPMailer();
						        $email->setLanguage('br');
						        //$email->isSMTP();
						        //$email->Host = 'smtp.villarreal.com.br';
						       	//$email->SMTPAuth = true;
						        //$email->Port = '587';
						        //$email->Username = 'webmaster@villarreal.com.br';
						        //$email->Password = '#zaraGoza2016';

						        $email->From = $emailremetente;
						        //$email->Sender = $emailremetente;
						        $email->FromName = utf8_decode($from_name);
						        //$email->ReturnPath = $emailremetente;
						        $email->addAddress($emaildestinatario1);
						        //$email->addAddress($emaildestinatario2);

						        $email->WordWrap = 50;
						        $email->isHTML(true);
						        $email->Subject = utf8_decode($Subject);
						        $email->Body  = $body;
						        $email->AltBody = 'Para visualizar este e-mail corretamente use o visualizador de e-mail com suporte a HTML';

						        echo "<div class='alert alert-success alert-dismissible'>
					            	<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
					                <h4>Sucesso</h4>
					               	Requerimento realizado com sucesso
					            </div>";

						        if($email->Send()){
						            /*  
						            echo "<div class='form-group'>
						                <label class='col-sm-12 control-label' style='color: #00a65a !important;'>E-mail enviado com sucesso</label>
						             </div>";
						            */
						        }else{
						            /*        
						            echo "<div class='form-group'>
						                <label class='col-sm-12 control-label' style='color: #f39c12 !important;'>Erro ao enviar e-mail</label>
						            </div>";
						            */
						        }

							}else{

								echo "<div class='alert alert-warning alert-dismissible'>
					            	<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
					                <h4>Erro</h4>
					               	Erro ao realizar inscrição
					            </div>";

							}
						}
					}
				}

	?>

	<div class='control no-padding-bottom'>
		<?php include_once('includes/about-content.php'); ?>
	</div>

	<div class='control no-padding-top videos'>
		<?php include_once('includes/about-videos.php'); ?>
	</div>
	
</section>