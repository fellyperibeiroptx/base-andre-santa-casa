<?php 
	$getOuvidoria = new Read();
	$getOuvidoria->fullRead("SELECT * FROM ".PREFIX."paginas WHERE url_amigavel = 'ouvidoria'");
	$ouvidoria = $getOuvidoria->getResult()[0]['descricao'];

	$getTrabalheConosco = new Read();
	$getTrabalheConosco->fullRead("SELECT * FROM ".PREFIX."paginas WHERE url_amigavel = 'trabalhe_conosco'");
	$trabalhe_conosco = $getTrabalheConosco->getResult()[0]['descricao'];
?>

<div class='bloco-conteudo localizacao'>

	<div class='conteudo-paginas-padding' style="padding-top: 40px;">

		<div class="col-md-4 col-sm-12">
			<center>
				<img src="<?php echo ROOT; ?>resources/img/localizacao02.svg" alt="Localização - Santa Casa de Lorena" class="img-responsive" alt='Localização - Santa Casa de Lorena' style='max-width: 53px;'>
				<h2>Localização</h2>
				<h3 class="fonte2"><?php echo $localizacao['localizacao']; ?></h3>
			</center>
		</div>

		<div class="col-md-4 col-sm-12">
			<center>
				<img src="<?php echo ROOT; ?>resources/img/telefone02.svg" alt="Localização - Santa Casa de Lorena" class="img-responsive" alt='Telefone - Santa Casa de Lorena' style='max-width: 53px;'>
				<h2>Telefone</h2>
				<h3 class="fonte2"><?php echo $localizacao['telefone']; ?></h3>
			</center>
		</div>

		<div class="col-md-4 col-sm-12">
			<center>
				<img src="<?php echo ROOT; ?>resources/img/email02.svg" alt="Email - Santa Casa de Lorena" class="img-responsive" alt='E-mail - Santa Casa de Lorena' style='max-width: 53px;'>
				<h2>E-mail</h2>
				<h3 class="fonte2"><?php echo $localizacao['email']; ?></h3>
			</center>
		</div>

	</div>

	<div class="clear"></div>
</div>

<div class='bloco-conteudo'>
	<div class='conteudo-paginas-padding bg-cinza form-contato'>

		<div class="col-md-4 col-sm-12 titulo">
			<h2>entre em contato</h2>
			<div class="barra">
				<div class="parte1"></div>
				<div class="parte2"></div>
			</div>
		</div>

		<?php 

		if(isset($dados['g-recaptcha-response'])){

			// verifique a chave secreta
			$reCaptcha = new ReCaptcha($secret);

			$response = $reCaptcha->verifyResponse(
		        $_SERVER["REMOTE_ADDR"],
		        $_POST["g-recaptcha-response"]
		    );

			if($response != null && $response->success) {

				$createContato = new Create;
				unset($dados['g-recaptcha-response']);
				$dados['data_cadastro'] = date("Y-m-d H:i:s");

				if(isset($dados['form']) && $dados['form'] == "contato"){
					unset($dados['form']);

					$createContato->ExeCreate(PREFIX."ouvidoria", $dados);

					if($createContato->getResult()){

						$Message = EMKT_HEADER."<table width='700' border='0' cellspacing='30' cellpadding='0' class='conteudo white' align='center' bgcolor='#FFFFFF'>
							<tr>
								<td align='center'>
									<img width='200' src='http://www.santacasalorena.org.br/novo/resources/img/logo.jpg'>
								</td>
							</tr>
							<tr>
								<td align='center'>
									<h2>Um novo contato foi realizado através do site</h2>
								</td>
							</tr>
							<tr>
								<td class=''><hr></td>
							</tr>
							<tr>
								<td>
									<h2>NOME</h2>
									<p>{$dados['nome']}</p>
								</td>
							</tr>
							<tr>
								<td>
									<h2>E-MAIL</h2>
									<p>{$dados['email']}</p>
								</td>
							</tr>
							<tr>
								<td>
									<h2>CIDADE</h2>
									<p>{$dados['cidade']}</p>
								</td>
							</tr>
							<tr>
								<td>
									<h2>ASSUNTO</h2>
									<p>{$dados['assunto']}</p>
								</td>
							</tr>
							<tr>
								<td>
									<h2>RAZÃO</h2>
									<p>{$dados['razao']}</p>
								</td>
							</tr>
							<tr>
								<td>
									<h2>MENSAGEM</h2>
									<p>{$dados['mensagem']}</p>
								</td>
							</tr>
						</table>".EMKT_FOOTER;
			       
				        $body = utf8_decode($Message);
				        $mail = new PHPMailer();
				        $nomeRemetente = 'Santa Casa de Lorena';
				        $mail->Subject =  utf8_decode("Ouvidoria - Novo contato através do site");

				        if(HOME == 'http://172.16.0.123/SCL/'):

				          $usuario = 'pentaxialdev@gmail.com';
				          $To = $_POST['email'];

				          $mail->SetFrom($usuario, utf8_decode($nomeRemetente));
				          $mail->IsSMTP();
				          //$mail->SMTPDebug = 6;
				          $mail->SMTPSecure = 'tls'; 
				          $mail->Port = 25; //Indica a porta de conexão para a saída de e-mails
				          $mail->Host = 'smtp.gmail.com'; //smtp.dominio.com.br
				          $mail->SMTPAuth = true; //define se haverá ou não autenticação no SMTP
				          $mail->Username = $usuario;
				          $mail->Password = 'PX705co*10';
				      
				        else:

				          	$usuario = 'webmaster@santacasalorena.org.br';
				        	$To = $localizacao['email'];

				        endif;

				        $mail->SetFrom($usuario, utf8_decode($nomeRemetente));

				        $mail->MsgHTML($body);
				        $mail->AddAddress($To, "");

				        if($mail->Send()){
				        	echo "<div class='col-md-12 col-sm-12'><label class='msg-erro' style='color:#00a65a !important;'>E-mail enviado com sucesso</label></div>";
				        }else{
				        	echo "<div class='col-md-12 col-sm-12'><label class='msg-erro'>Erro ao enviar e-mail</label></div>";
				        }
				    }else{
				    	echo "<div class='col-md-12 col-sm-12'><label class='msg-erro'>Erro ao enviar e-mail</label></div>";
				    }

				}elseif(isset($dados['form']) && $dados['form'] == "trabalhe_conosco"){
					unset($dados['form']);

					$getCurriculum= new Upload("arquivos");
					$getCurriculum->File($_FILES['curriculum-tc'], Check::urlAmigavel($dados['nome']."_".date("dmY")), "/curriculuns", null);

					if($getCurriculum->getResult()){

						$dados['curriculum'] = $getCurriculum->getResult();
						$createContato->ExeCreate(PREFIX."trabalhe_conosco", $dados);

						if($createContato->getResult()){

							$Message = EMKT_HEADER."<table width='700' border='0' cellspacing='30' cellpadding='0' class='conteudo white' align='center' bgcolor='#FFFFFF'>
								<tr>
									<td align='center'>
										<img width='200' src='http://www.santacasalorena.org.br/novo/resources/img/logo.jpg'>
									</td>
								</tr>
								<tr>
									<td align='center'>
										<h2>Um novo contato de TRABALHO CONOSCO foi realizado através do site</h2>
									</td>
								</tr>
								<tr>
									<td class=''><hr></td>
								</tr>
								<tr>
									<td>
										<h2>NOME</h2>
										<p>{$dados['nome']}</p>
									</td>
								</tr>
								<tr>
									<td>
										<h2>E-MAIL</h2>
										<p>{$dados['email']}</p>
									</td>
								</tr>
								<tr>
									<td>
										<h2>CIDADE</h2>
										<p>{$dados['cidade']}</p>
									</td>
								</tr>
							</table>".EMKT_FOOTER;
					       
					        $body = utf8_decode($Message);
					        $mail = new PHPMailer();
					        $nomeRemetente = 'Santa Casa de Lorena';
					        $mail->Subject =  utf8_decode("Trabalhe Conosco - Novo contato através do site");

					        if(HOME == 'http://172.16.0.123/SCL/'):

					          $usuario = 'pentaxialdev@gmail.com';
					          $To = $_POST['email'];

					          $mail->SetFrom($usuario, utf8_decode($nomeRemetente));
					          $mail->IsSMTP();
					          //$mail->SMTPDebug = 6;
					          $mail->SMTPSecure = 'tls'; 
					          $mail->Port = 25; //Indica a porta de conexão para a saída de e-mails
					          $mail->Host = 'smtp.gmail.com'; //smtp.dominio.com.br
					          $mail->SMTPAuth = true; //define se haverá ou não autenticação no SMTP
					          $mail->Username = $usuario;
					          $mail->Password = 'PX705co*10';
					      
					        else:

					          	$usuario = 'webmaster@santacasalorena.org.br';
					        	$To = $localizacao['email'];

					        endif;

					        $mail->SetFrom($usuario, utf8_decode($nomeRemetente));

					        $mail->MsgHTML($body);
					        $mail->AddAddress($To, "");
					        $mail->AddAttachment($getCurriculum->getResult());

					        if($mail->Send()){
					        	echo "<div class='col-md-12 col-sm-12'><label class='msg-erro' style='color:#00a65a !important;'>E-mail enviado com sucesso</label></div>";
					        }else{
					        	echo "<div class='col-md-12 col-sm-12'><label class='msg-erro'>Erro ao enviar e-mail</label></div>";
					        }
					    }else{
					    	echo "<div class='col-md-12 col-sm-12'><label class='msg-erro'>Erro ao enviar e-mail</label></div>";
					    }
				    }else{
				    	echo "falha no upload do arquivo ou arquivo é válido";
				    }
				}
			}else{
				echo "<div class='col-md-12 col-sm-12'><label class='msg-erro'>PREENCHA O CAPTCHA CORRETAMENTE</label></div>";
			}
		}

		?>

		<div class="col-md-12 col-sm-12 fonte2" id="bloco_ouvidoria_texto"><?php echo $ouvidoria; ?></div>
		<div class="col-md-12 col-sm-12 fonte2" id="bloco_trabalhe-conosco_texto" style="display: none;"><?php echo $trabalhe_conosco; ?></div>

		<div class="col-md-12 col-sm-12">

			<div class='btn-fale_conosco contato-ativo' id="ouvidoria" style="max-width: 190px;">
				<div class='icon ouvidoria'></div>
				<p>Ouvidoria</p>
				<span class="seta-baixo"></span>
			</div>

			<div class='btn-fale_conosco' id="trabalhe-conosco">
				<div class='icon trabalhe-conosco'></div>
				<p>Trabalhe conosco</p>
				<span class="seta-baixo"></span>
			</div>
		</div>

		<div class="col-md-12 col-sm-12" id="bloco_ouvidoria" style="padding-left: 0; padding-right: 0; ">
			<form method="post" id="form_contato" action="<?php echo (isset($_SERVER['QUERY_STRING']) ? HOME.str_replace("qs=", "", $_SERVER['QUERY_STRING']) : "" ); ?>">
				
				<input type="hidden" name="form" value="contato">
				<input type="hidden" id="ouvidoria-recaptcha" name="g-recaptcha-response">

				<div class="form-group col-md-4 col-sm-12">
	                <input class="form-control" name="nome" id='nome-o' placeholder="nome*">
	                <label class="msg-erro"></label>
	            </div>

	            <div class="form-group col-md-4 col-sm-12">
	                <input class="form-control" name="email" id="email-o" placeholder="email*">
	                <label class="msg-erro"></label>
	            </div>

	            <div class="form-group col-md-4 col-sm-12">
	                <input class="form-control" name="cidade" id='cidade-o' placeholder="cidade*">
	                <label class="msg-erro"></label>
	            </div>

	            <div class="form-group col-md-6 col-sm-12">
	                <input class="form-control" name="assunto" id='assunto-o' placeholder="assunto*">
	                <label class="msg-erro"></label>
	            </div>

	            <div class="form-group col-md-6 col-sm-12">
	                <input class="form-control" name="razao" id='razao-o' placeholder="razao para contato*">
	                <label class="msg-erro"></label>
	            </div>

	            <div class="form-group col-md-12 col-sm-12">
	                <textarea class="form-control" name="mensagem" id='mensagem-o' placeholder="mensagem*" style="height: 250px;" ></textarea>
	                <label class="msg-erro"></label>
	            </div>

            	<input type="submit" class='btn-fale_conosco' value="enviar">
            </form>
		</div>

		<div class="col-md-12 col-sm-12" id="bloco_trabalhe-conosco" style="padding-left: 0; padding-right: 0; display: none;">
			<form method="post" id="form_trabalhe-conosco" action="<?php echo (isset($_SERVER['QUERY_STRING']) ? HOME.str_replace("qs=", "", $_SERVER['QUERY_STRING']) : "" ); ?>" enctype="multipart/form-data">
				
				<input type="hidden" name="form" value="trabalhe_conosco">
				<input type="hidden" id="trabalhe-conosco-recaptcha" name="g-recaptcha-response">

				<div class="form-group col-md-4 col-sm-12">
	                <input class="form-control" name="nome" id='nome-tc' placeholder="nome*">
	                <label class="msg-erro"></label>
	            </div>

	            <div class="form-group col-md-4 col-sm-12">
	                <input class="form-control" name="email" id='email-tc' id="email" placeholder="email*">
	                <label class="msg-erro"></label>
	            </div>

	            <div class="form-group col-md-4 col-sm-12">
	                <input class="form-control" name="cidade" id='cidade-tc' placeholder="cidade*">
	                <label class="msg-erro"></label>
	            </div>

	            <div class="form-group col-md-12 col-sm-12" style="min-height: 40px;">
                  <label for="exampleInputFile">Curriculum (PDF)</label>
                  <input type="file" name='curriculum-tc' id='curriculum-tc' style="height: 20px;">
                  <label class="msg-erro"></label>
                </div>

	            <input type="submit" class='btn-fale_conosco' value="enviar">
	        </form>
		</div>

	    <div style="padding:0 15px;"><div class="g-recaptcha" data-sitekey="6LfM4TkUAAAAABw-GkATRquqIuSgp_KSKYN_XWjO"></div></div>
	    <div class="clear"></div>

	</div>

	<div>

		<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDlhyOX-3idOmnw4PDc052pZUL24iWKhBA"></script>
		<script>

			var geocoder;
			var map;
			function initialize() {
			  geocoder = new google.maps.Geocoder();
			  var latlng = new google.maps.LatLng(14.2392976, -53.1805017);
			  var mapOptions = {
			    zoom: 18,
			    center: latlng
			  }
			  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
			  codeAddress();
			}

			function codeAddress() {
			  var address = document.getElementById('address').value;
			  geocoder.geocode( { 'address': address}, function(results, status) {
			    if (status == google.maps.GeocoderStatus.OK) {
			      map.setCenter(results[0].geometry.location);
			      var marker = new google.maps.Marker({
			          map: map,
			          position: results[0].geometry.location
			      });
			    } else {
			      alert('Os dados de geocalização não conferem: ' + status);
			    }
			  });
			}

			google.maps.event.addDomListener(window, 'load', initialize);

    	</script>

        <input type="hidden" id="address" value="<?php echo $localizacao['localizacao']; ?>">
        <div id="map-canvas"></div>

	</div>

</div>

<script type="text/javascript">
	
	$(document).ready(function(){

		$( "#ouvidoria" ).on( "click", function() {

			$("#trabalhe-conosco").removeClass("contato-ativo");
			$("#bloco_trabalhe-conosco").fadeOut();

			$(this).addClass("contato-ativo");
			$("#bloco_ouvidoria").fadeIn();
		});

		$( "#trabalhe-conosco" ).on( "click", function() {

			$("#ouvidoria").removeClass("contato-ativo");
			$("#bloco_ouvidoria").fadeOut();

			$(this).addClass("contato-ativo");
			$("#bloco_trabalhe-conosco").fadeIn();
		});
	});

	$("#form_contato").on('submit', function (e) {

		$("#ouvidoria-recaptcha").val($("#g-recaptcha-response").val());

	    e.preventDefault();

		$(".form-group").removeClass("has-warning").find(".msg-erro").attr("style","").text("");

	    if($('#nome-o').val() == ""){

	        $('#nome-o').focus().closest(".form-group").addClass("has-warning").find(".msg-erro").fadeIn().text("Insira um nome válido");
	    }else if($('#email-o').val() == "" || !validaEmail($('#email-o').val())){

	        $('#email-o').focus().closest(".form-group").addClass("has-warning").find(".msg-erro").fadeIn().text("Insira um e-mail válido");
	    }else if($('#cidade-o').val() == ""){

	        $('#cidade-o').focus().closest(".form-group").addClass("has-warning").find(".msg-erro").fadeIn().text("Insira um cidade válida");
	    }else if($('#assunto-o').val() == ""){

	        $('#assunto-o').focus().closest(".form-group").addClass("has-warning").find(".msg-erro").fadeIn().text("Insira um assunto válido");
	    }else if($('#razao-o').val() == ""){

	        $('#razao-o').focus().closest(".form-group").addClass("has-warning").find(".msg-erro").fadeIn().text("Insira uma razão válida");
	    }else if($('#mensagem-o').val() == ""){

	        $('#mensagem-o').focus().closest(".form-group").addClass("has-warning").find(".msg-erro").fadeIn().text("Insira uma mensagem válida");
	    }else{
	                
	        $(this).off('submit').submit();  
	    }
	});

	$("#form_trabalhe-conosco").on('submit', function (e) {

		$("#trabalhe-conosco-recaptcha").val($("#g-recaptcha-response").val());

	    e.preventDefault();

		$(".form-group").removeClass("has-warning").find(".msg-erro").attr("style","").text("");

	    if($('#nome-tc').val() == ""){

	        $('#nome-tc').focus().closest(".form-group").addClass("has-warning").find(".msg-erro").fadeIn().text("Insira um nome válido");
	    }else if($('#email-tc').val() == "" || !validaEmail($('#email-tc').val())){

	        $('#email-tc').focus().closest(".form-group").addClass("has-warning").find(".msg-erro").fadeIn().text("Insira um e-mail válido");
	    }else if($('#cidade-tc').val() == ""){

	        $('#cidade-tc').focus().closest(".form-group").addClass("has-warning").find(".msg-erro").fadeIn().text("Insira uma cidade válida");
	    }else if(!fileValidation(document.getElementById('curriculum-tc'))){

	    	$('#curriculum-tc').focus().closest(".form-group").addClass("has-warning").find(".msg-erro").fadeIn().text("Insira um arquivo válido(PDF)");
	    }else{
	                
	        $(this).off('submit').submit();  
	    }
	});

	/*
	function fileValidation(){
	    var fileInput = document.getElementById('curriculum-tc');
	    var filePath = fileInput.value;
	    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
	    if(!allowedExtensions.exec(filePath)){
	        alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
	        fileInput.value = '';
	        return false;
	    }else{
	        //Image preview
	        if (fileInput.files && fileInput.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function(e) {
	                document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'"/>';
	            };
	            reader.readAsDataURL(fileInput.files[0]);
	        }
	    }
	}
	*/

	function fileValidation(elemento){
	    var fileInput = elemento;
	    var filePath = fileInput.value;
	    var allowedExtensions = /(\.pdf)$/i;
	    if(!allowedExtensions.exec(filePath)){
	        fileInput.value = '';
	        return false;
	    }else{
	        return true;
	    }
	}

</script>