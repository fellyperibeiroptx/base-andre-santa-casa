<?php 

$getCursosPG = new Read();
$getCursosPG->fullRead("SELECT C.*, DATE_FORMAT(T.data, '%d/%m/%Y') AS data_inicio
							FROM ".PREFIX."curso AS C
							LEFT JOIN ".PREFIX."turma AS T ON (T.id_curso = C.id_curso)
							WHERE C.tipo = 'Pós-Graduação' AND C.status = 1 GROUP BY C.id_curso");

if($getCursosPG->getResult()):
?>
	<div id='pos-graduacao' class='course-title cl-white padding-bottom padding-top'>
		<div class='to-left'>
			<h2 class='cl-white'>Cursos de Pós Graduação </h2>
			<hr class='bg-white'>
			<p class='cl-white'>A graduação forma o profissional generalista e competente para ingressar no mercado de trabalho. A pós graduação transforma conhecimento em concretização, acelera e direciona o crescimento profissional.</p>
		</div>
		<div class='to-right'>
			<button class='normal-btn open-course-list-pos bg-white hv-bg-gray-light'><span class='cl-gradient-blue'>Ver todos</span></button>
		</div>
		<div class='clear-both'></div>
	</div>
	<div class='clear-both'></div>

	<!-- carousel -->
	<div class='intern-control owl-carousel owl-courses-pos'>
		
		<?php 

			foreach ($getCursosPG->getResult() AS $key => $curso) {

				echo "<div>
					<a href='javascript:getCurso(\"{$curso['url_amigavel']}\")'>
						<div class='owl-course-item course-info hv-bg-white cl-white hv-cl-blue'>
							<div class='control-image'>
								<img src='{$curso['img']}'>
								<div class='hover-button'>
									<button class='normal-btn bg-gradient-green hv-bg-gradient-green-inverse'><span class='cl-white'>Conheça o curso</span></button>
								</div>
							</div>
							<h3 class='cl-white'>{$curso['titulo']}</h3>
							<h4>".($curso['data_inicio'] != "" ? "Início: {$curso['data_inicio']}</h4>" : "Em breve")."
						</div>
					</a>
				</div>"; 
			}
		?>

	</div>
	<!-- end | carousel -->
<?php endif; ?>