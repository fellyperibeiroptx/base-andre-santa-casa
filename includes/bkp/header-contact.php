<div class='header-contact bg-gradient-blue'>
	<div class='control'>
		<div class='to-left to-icon'>
			<a href='https://www.facebook.com/neacursos/' target='_blank'>
				<div class=' social-icon social-icon-facebook bg-white radius cl-blue hv-cl-blue-dark'><i class='fab fa-facebook-f'></i></div>
			</a>
			<a href='https://www.instagram.com/neacursos/' target='_blank'>
				<div class=' social-icon social-icon-instagram bg-white radius cl-blue hv-cl-blue-dark'><i class='fab fa-instagram'></i></div>
			</a>
			<a href='https://www.youtube.com/channel/UCLt1XdduFlkzW14ZAvMsBnA' target='_blank'>
				<div class=' social-icon social-icon-youtube bg-white radius cl-blue hv-cl-blue-dark'><i class='fab fa-youtube'></i></div>
			</a>
			<a  href='http://api.whatsapp.com/send?1=pt_BR&phone=5512991215593' target='_blank'>
				<div style="color:#25d366" class=' social-icon social-icon-whatsapp bg-white radius cl-blue hv-cl-blue-dark'><i class='fab fa-whatsapp'></i></div>
			</a>
		</div>
		<div class='to-contact to-right cl-white'>(12) 3621-8408 / <i class="fab fa-whatsapp" style="color:#25d366"></i> (12) 9 9121-5593 / contato@neacursos.com.br</div>
	</div>
</div>