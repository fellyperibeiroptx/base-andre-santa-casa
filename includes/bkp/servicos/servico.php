<?php

	require('../../_app/Config.inc.php');
	
	//Pego dados post
	$dados = filter_input_array(INPUT_POST, FILTER_DEFAULT);
	
	if(isset($dados['acao'])):

		//Apaga categoria do produto
		switch ($dados['acao']) {

			case 'GetCurso':

				$url = $dados['curso'];

				$ReadCurso = new Read();
				$ReadCurso->fullRead("SELECT * FROM ".PREFIX."curso WHERE url_amigavel = :curso", "curso={$url}");
				
				//var_dump($ReadCurso);

				if($ReadCurso->getResult()){

					$response = $ReadCurso->getResult()[0];
					
					//Pega corpo decente
					$readCorpoDocente = new Read;
                    $readCorpoDocente->fullRead("SELECT CD.* FROM ".PREFIX."corpo_docente AS CD
                                            INNER JOIN ".PREFIX."curso_corpo_docente AS CCD ON (CD.id_corpo_docente = CCD.id_corpo_docente) 
                                            WHERE CCD.id_curso = :id_curso", "id_curso={$response['id_curso']}"); 
                            
                    if($readCorpoDocente->getResult()): 
                        $response['corpo_docente'] = $readCorpoDocente->getResult();
                    endif;

                    //Pega coordenadores
					$readCoordenadores = new Read;
                    $readCoordenadores->fullRead("SELECT CD.* FROM ".PREFIX."corpo_docente AS CD
                                            INNER JOIN ".PREFIX."curso_coordenacao AS CC ON (CD.id_corpo_docente = CC.id_corpo_docente) 
                                            WHERE CC.id_curso = :id_curso", "id_curso={$response['id_curso']}"); 
                            
                    if($readCoordenadores->getResult()): 
                        $response['coordenacao'] = $readCoordenadores->getResult();
                    endif;

                    //Pega matriz curricular
					$getMatriz = new Read();
                    $getMatriz->fullRead("SELECT * FROM ".PREFIX."matriz_curricular WHERE id_curso = {$response['id_curso']}");

                    if($getMatriz->getResult()):
                        $response['matriz_curricular'] = $getMatriz->getResult();
                    endif;

                    $getTurma = new Read();
                    $getTurma->fullRead("SELECT *, DATE_FORMAT(data, '%d/%m/%Y') AS data FROM ".PREFIX."turma WHERE id_curso = {$response['id_curso']} AND status = 1 ORDER BY data DESC LIMIT 1");

                    if($getTurma->getResult()){
                    	$response['data_inicio'] = $getTurma->getResult()[0]['data'];
                    }

                    //DATE_FORMAT(data, '%d/%m/%Y') AS data

					echo json_encode($response);

				}else{

					echo 0;
				}

			break;

			case 'getCursosPorTipo':

				$tipo = $dados['tipo'];

				$getInscricoesAbertas = new Read();
				$getInscricoesAbertas->fullRead("SELECT C.titulo, C.url_amigavel, DATE_FORMAT(T.data, '%d/%m/%Y') AS data_inicio 
													FROM ".PREFIX."curso AS C
													INNER JOIN ".PREFIX."turma AS T ON (T.id_curso = C.id_curso)
													WHERE C.tipo = :tipo AND T.data >= NOW() AND C.status = 1 AND T.status = 1 ORDER BY T.data ASC",
													"tipo={$tipo}");

				if($getInscricoesAbertas->getResult()):
					echo json_encode($getInscricoesAbertas->getResult());
				else:
					echo 0;
				endif;

			break;

		}

	endif;
?>