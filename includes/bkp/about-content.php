<h2>Sobre o NEA Cursos<span class='cl-blue'>.</span></h2>
<h3>Palestras, cursos de extensão e de pós-graduação lato sensu (pós graduação).</h3>
<hr class='warp bg-gradient-blue'>
<div class='intern-control'>
	<p>O NEA, Núcleo de Estudos Avançados, é uma empresa privada fundada em 2010 por professores universitários com mais de vinte anos de experiência em docência na graduação, aprimoramento profissional e pós-graduação lato e stricto sensu. Desde a fundação vem se consolidando na região como instituição de alta qualidade de ensino, reconhecida pela comunidade acadêmica. Aprimore seus conhecimentos, desenvolva suas habilidades técnicas, amplie seu network e valorize seu tempo.</p>
	<div class='margin-bottom'>
		<div class='to-left half bg-gray-light'>
			<img src='resources/img/puc.png' alt='PUC Goiás'>
			<p>A partir de 2014 os cursos de pós graduação são oferecidos pelo NEA em parceria com a Pontifícia Universidade Católica de Goiás – PUC Goiás. Este convênio vem fortalecer o compromisso com a qualidade assumida desde a fundação da empresa.</p>
		</div>
		<div class='to-right half no-padding no-margin-bottom'>
			<h4>Visão<span class='cl-blue'>.</span></h4>
			<p>Ser referência em cursos de especialização e extensão na área da saúde, dispondo de corpo docente altamente qualificado, ampliando continuamente as possibilidades de ofertas de formação acadêmica e profissional</p>
			<h4>Missão<span class='cl-blue'>.</span></h4>
			<p>Por meio da excelência na difusão do conhecimento oferecer aprimoramento acadêmico em ambiente de crescimento, respeito e inovação que contribua para o desenvolvimento profissional.</p>
			<h4>Valores<span class='cl-blue'>.</span></h4>
			<p>Ética, respeito, compromisso, transparência e busca responsável pelos objetivos.</p>
		</div>
		<div class='clear-both'></div>
	</div>
</div>