<div class='header-banner'>
	<div class='owl-carousel owlBANNER'>

		<?php 
			$getBanner = new Read();
			$getBanner->fullRead("SELECT * FROM ".PREFIX."banner WHERE status = 1");

			if($getBanner->getResult()):
				foreach($getBanner->getResult() AS $key => $banner):
		?>
					<div>
						<div class='imgBanner' style=' background: url(<?php echo $banner['img'] ; ?>) no-repeat center center / cover ;'></div>
					</div>
		<?php 
				endforeach;
			endif;
		?>
		
	</div>
</div>