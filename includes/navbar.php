<header>
  <div id="navbar">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
      <div class="navbar-header">
        <a class="navbar-brand" href="<?php echo ROOT; ?>">
          <img src="<?php echo ROOT; ?>resources/img/logo.svg" alt="Santa Casa Lorena" class="logo-scl">
        </a>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <!--Opçõe-->
      <div class="collapse navbar-collapse menu-principal" id="navbar-collapse-1">
        <ul class="nav navbar-nav pull-right">
          
          <li>
            <a id="menu-item-home" href="<?php echo ROOT; ?>">Home</a> 
          </li>

          <li class="dropdown">
            <a id="menu-item-institucional" href="#" class="dropdown-toggle" data-toggle="dropdown">Institucional <i class="arrow down"></i></a> 
            <ul class="dropdown-menu">
              <li><a href="<?php echo ROOT; ?>institucional/sobre-a-santa-casa">Sobre a santa casa</a></li>
              <li><a href="<?php echo ROOT; ?>institucional/humanizacao">Humanização</a></li>
              <li><a href="<?php echo ROOT; ?>institucional/acoes-sociais-ambientais">Ações sociais e ambientais</a></li>
              <li><a href="<?php echo ROOT; ?>institucional/programa-nacional-seguranca">Programa nacional de segurança do paciente</a></li>
              <li><a href="<?php echo ROOT; ?>institucional/portal-transparencia">Portal da transparência</a></li>
            </ul>
          </li>

          <li class="dropdown">
            <a id="menu-item-instalacoes" href="#" class="dropdown-toggle" data-toggle="dropdown">Instalações <i class="arrow down"></i></a> 
            <ul class="dropdown-menu">
              <li><a href="<?php echo ROOT; ?>instalacoes/pronto-atendimento">Pronto atendimento</a></li>
              <li><a href="<?php echo ROOT; ?>instalacoes/hotelaria">Hotelaria</a></li>
              <li><a href="<?php echo ROOT; ?>instalacoes/clinica-emilia">Clínica Emília</a></li>
              <li><a href="<?php echo ROOT; ?>instalacoes/centro-diagnostico-por-imagem">Centro de diagnóstico por imagem</a></li>
              <li><a href="<?php echo ROOT; ?>instalacoes/unidades-de-internacao">Unidade de internação</a></li>
            </ul>
          </li>

          <li class="dropdown">
            <a  id="menu-item-servicos" href="#" class="dropdown-toggle" data-toggle="dropdown">Serviços <i class="arrow down"></i></a> 
            <ul class="dropdown-menu">
              <li><a href="<?php echo ROOT; ?>servicos/convenios">Convênios</a></li>
              <li><a href="<?php echo ROOT; ?>servicos/especialidades">Especialidades</a></li>
              <li><a href="<?php echo ROOT; ?>servicos/capacidade-instalacao-producao">Capacidade de instalação e produção</a></li>
              <li><a href="<?php echo ROOT; ?>servicos/manual-do-paciente-e-visitantes">Manual do paciente e visitante</a></li>
            </ul>
          </li>

          <li>
            <a id="menu-item-noticias" href="<?php echo ROOT; ?>noticias">Notícias</a> 
          </li>

          <li>
            <a id="menu-item-fale-conosco" href="<?php echo ROOT; ?>fale-conosco">Fale Conosco</a> 
          </li>

          <li>
            <a href="<?php echo ROOT; ?>doacoes">
              <span class='btn-doacao'>
                Doação
              </span>
            </a> 
          </li>

          <li class="btn-facebook">
            <a href='https://www.facebook.com/santacasadelorena/' target="_blank">
              <span>
                <i class="fa fa-fw fa-facebook"></i>
              </span>
            </a>
          </li>

        </ul>
      </div>
      <!-- /.navbar-collapse -->

    </nav>
  </div>
</header>