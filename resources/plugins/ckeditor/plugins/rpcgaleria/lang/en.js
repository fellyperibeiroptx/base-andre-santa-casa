﻿CKEDITOR.plugins.setLang( 'rpcgaleria', 'en', {
    button: 'Insert HTML5 audio',
    title: 'Galeria de Imagens',
    infoLabel: 'Insira/Selecione uma galeria',
    urlMissing: 'Audio source URL is missing.',
    audioProperties: 'Audio properties'
} );
