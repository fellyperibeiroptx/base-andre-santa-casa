$(document).ready(function() {
   $('.owl-banner').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        items: 1
    });

    $('.owl-atendimento').owlCarousel({
        center: true,
        items:4,
        loop:true,
        margin:1
    });

});